In this first (short) Lab, we'll help you set up your environment correctly, so that you can be quickly and efficiently productive in the next Labs.

# A little theory about Git Branching

## In general

Git branches are a way to work on a "copy" of your code, while maintaining the tracking of the changes you make. This can be very useful to test a new feature: you "copy" the current state of your code (== create a branch), then your write new code/modify existing code. If you're satisfied with your code, you can "replace" the initial code with the content of the branch (== merge the branch) so that the code in the branch becomes the new version of your code (while preserving the history of each small modification you made inside the branch). If you're not satisfied, you can simply drop the branch (== switch back to the initial code and/or delete the branch).
Branches can also be very useful when multiple persons are working on the code. Indeed, each developer can work on her own copy of the original code without interferring with (or being interferred by) the modifications that other developers are performing on the some pieces of code. Only when one of the devs will push her work in the original branch, she will have to worry about possible changes that other developers made in the same pieces of code (and eventually will have to resolve conflicts).

Git allows you to create as many branches as you want, whenever you want. However, over time, very good developers ("gurus") have developed some "best practices" on how to manage (git) branches.

[This article](https://nvie.com/posts/a-successful-git-branching-model/) provides a very good introduction to one of the most commonly used git branching model.

To sum up, they propose to define the following branches:

- master: this is the branch where the source code **always reflects a *production-ready* state**. Anyone that connects to your repository and downloads a snapshot of your code (e.g. zip file) should get code from this branch and should enter in possession of a fully tested and functional program. This branch exists from the beginning of the project and never disappears.

- develop: this is the branch where the source code **always reflects a state with the latest delivered development changes for the next release**. Some would call this the "integration branch". This is where any automatic nightly builds (alhas & betas versions of the program) are built from. This branch exists from the beginning of the project and never disappears.

- release: these branches (there might exist several) support preparation of a **new production release**. They allow for minor bug fixes and preparing meta-data for a release (version number, build dates, etc.), at the same time as clearing the develop branch to receive features for the next big release. Such branches are created from the develop branch when developers want to create a new version of the program and might never be destroyed. [TODO: OK?]

- feature: these branches (there might exist several) are used to develop new features for the upcoming or a distant **future release**.  Such branches are temporary. They are created from the develop branch and are finally either merged back in the develop branch or abandonned.

- hotfixes: these are very specific branches. The are similar to release branches in that they are also meant to prepare for a new production release, albeit unplanned. They arise from the necessity to act immediately upon an undesired state of a **live production version** (thus they are branched off from *master*). When a critical bug in a production version must be resolved immediately, a hotfix branch may be branched off from the corresponding tag on the master branch that marks the production version.

### Resources

https://www.atlassian.com/git/tutorials/using-branches
https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell


## In this project

In this project, we use a much more simple version of this branching model. You'll create the following branches: 

- master (mandatory)
- development (mandatory)
- feature_XXX: everytime you need to develop a new feature, you'll create a new branch

# Tools

## Python

You'll write code in Python. Preferably install Python 3, as [Python 2.x is now deprecated](https://pythonclock.org/)).

You might find it useful to install the **[venv](https://docs.python.org/3/library/venv.html)** [package(https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/), in order to install external dependencies (pythno libraries you'll install and use later) in a specific directory, thus not messing with your potential current "system" installation of Python (that is used by the OS and might freeze the whole OS if you mess it up, for instance under GNU/Linux).

You can use whatever IDE/Editor (Development Environment) you wish. Some of the most commons are:
- IDLE (included with every Python installaiton)
- [Spyder](https://www.spyder-ide.org/)
- [PyCharm](https://www.jetbrains.com/pycharm/)
- [Visual Code](https://code.visualstudio.com/docs/languages/python)
- You can also use online editors like [Repl](https://repl.it/), but it is not recommended, as they might not have some of the libraries you'll need and you'll not be able to install them

## Git & Gitlab

You'll use Gitlab (through the [Official website](http://www.gitlab.com) or a [local installation](https://gitlabtelecom.dyndns.net/) -- if we manage to give access to this machine to you), both as your Git repository, and to run the CI (Continuous Integration) pipeline/workflow.

### **Exo1:** Git Repository Architecture

[TODO: encadrer les choses à faire]

### **Exo2:** GitLab Workflow/Pipeline

.gitlab-ci.yml

[yaml](https://en.wikipedia.org/wiki/YAML)
