# Introduction

Until now, during your courses at TSÉ, you've learned how to develop software (*Software Engineering*) and how to program in several languages (particularly in *C/C++/Python/Java*).

You also had a glimpse at how to test software with JUnit (during the *Software Engineering* and *Java* courses) and packaging with Maven (during the *Java* course).

However, as you've seen in the "Life Cycle" section of the *Software Engineering* course, in "real life", there's more to Software Development than just programming.

Indeed, Software Development follows a life cycle that goes from the *Ideation* step to the *Maintenance* step and loops back from there in iterative cycles. In case you don't remember, the various intermediary steps are:
1. Ideation & Feasibility
2. Analysis
3. Design
4. Programming/Refactoring
5. Unit Testing
6. Integration Testing
7. Validation Testing
8. Deployment & Distribution
9. Maintenance

Of course, as everybody knows, [developers are lazy](https://www.forbes.com/sites/forbestechcouncil/2018/01/08/developers-are-lazy-and-thats-usually-a-good-thing). As a consequence, along the years, they developed tools and methodologies to ease both: each of these steps, and the whole pipeline/workflow automation.

The main result of these improvements is called **DevOps**, as it mixes approaches from both **Dev**elopment (coding and testing) and **Op**eration**s** (installing & maintaining remote servers and daemons, deployment). It focuses on steps 4 to 9.

DevOps is the topic of the present course.

You're advised to read more [here](https://en.wikipedia.org/wiki/DevOps) and [here](https://youtu.be/M6F6GWcGxLQ) to learn about the context in which this course takes place.

The main steps of DevOps are the following:
1. **Coding**: code development and review
2. **Building**: continuous integration tools
3. **Testing**: continuous testing tools
4. **Packaging**: artifact repository
5. **Releasing**: release automation (continuous delivery/deployment)
6. **Configuring**: configuration, infrastructure as code tools
7. **Monitoring**: performance monitoring, end-user experience

You're advised to read more about those terms that you might not have encountered/understood in previous lectures, like:
* [**Continuous Integration (CI)**](https://en.wikipedia.org/wiki/Continuous_integration)
* [**Artifact**](https://en.wikipedia.org/wiki/Artifact_(software_development))
* [**Packaging**](https://en.wikipedia.org/wiki/Java_package) (remember, you've built [.jar](https://en.wikipedia.org/wiki/JAR_(file_format)) files with [Maven](https://en.wikipedia.org/wiki/Apache_Maven) during your Java projects!)
* [**Software Repository**](https://en.wikipedia.org/wiki/Software_repository) (remember, you've used the [Maven Repository](https://mvnrepository.com/repos/central) during your Java projects to download/update/manage external libraries, like OpenCV!)
* [**Continuous Deployment (CD)**](https://en.wikipedia.org/wiki/Continuous_deployment)
* [**Continuous Delivery (CDE)**](https://en.wikipedia.org/wiki/Continuous_delivery)
* [**Infrastructure as Code (IaC)**](https://en.wikipedia.org/wiki/Infrastructure_as_code)
* [**Monitoring**](https://en.wikipedia.org/wiki/Application_performance_management)

# This course

The present course will be an hands-on introduction to the DevOps domain, in which you'll dicover its various aspects by practicising them.

We've decided to make the various Labs integrate into a **global project**, that will act as a common thread to link the various topics we will address in this course and that will allow you to end up the course with a complete solution, that you'll be able to present to us.

You'll work by teams of 4 students (randomly selected).

In order to stay in the DevOps domain, we've decided that the topic of the project itself will be to from the DevOps domain: build a Server Monitoring tool (cf. next section for details).

The development of such a monitoring tool will allow you to practice *system programming* (in Python) and *server administration* (ssh connexion, parsing logs...), which are competences you've learned in other courses of this teaching block and are mandatory for DevOps Engineers. As well, while developing your code you'll learn (by practicising) the tools & methodologies to automate the process of programming/refactoring, testing, deploying and maintaining code, which are also mandatory competences for any DevOps Engineers.

Thus, you'll practice the DevOps methodology while building a DevOps tool. That's neat!

The course will be divided into a suite of ~12 Labs (~3 hrs each => 36 hrs total), so that you can see the many aspects & tools involved in the DevOps process, one at a time.

## Evaluation

The *evaluation* will take place during the last Lab.

- Each team will *pitch* & *demo* its production (40% of the final score).
- We will also review the teams' *source code* and *workflow* (60% of the final score).

# Monitoring Application

As Monitoring is an important part of DevOps, there already exists several turnkey[^turnkey] tools/tool-suites in the "market".

Two examples are [Grafana](https://grafana.com/) or [Beats + ELK Stack](https://www.elastic.co/what-is/elk-stack).

Of course, we will not use these tools, but develop our own (simplified) version. These tools will serve as examples for what your final result might look like.

For instance, below are two examples of monitoring dashboards, one in Grafana, the other with Beats+ELK-Stack:

| Example of Grafana Display | Example of Beats+ELK-Stack Dashboard |
|----------------------------|--------------------------------------|
| <img alt="Example of Grafana Display" src="./imgs/grafana.png" width="95%"/> | <img alt="Example of Beats+ELK-Stack Dashboard" src="./imgs/elk_monitoring.jpg" width="95%"/> |

The general functioning of a Monitoring platform is the following:

| General Overview of a Monitoring solution | Example of the Beats+ELK-Stack monitoring solution |
|----------------------------|--------------------------------------|
| <img alt="General overview of a monitoring solution" src="./imgs/unwanted_architecture.png" width="95%"/> | <img alt="Example of the Beats+ELK-Stack Monitoring solution" src="./imgs/architecture_simplified.png" width="95%"/> |

1. An "agent" (small piece of software that runs in the background) is installed on each machine to be monitored. This agent collects data about the server itself (general performance: CPU/RAM/Network/HardDrive/..., or security: logs/...) and/or a specific application, like a database or a web server (logs/...)
   + For instance in the ELK-Stack example, *Beats* is an agent that collects CPU/RAM/... data and *Logstash* is an agent that parses log files.
2. The agent sends the collected data to a central database
   + In the case of ELK-Stack, the Database is *ElasticSearch*
3. A graphical front-end extracts data from the database and synthetizes the info into multiple graphs, organized in dashboards, that provide an integrated view of the health of the monitored machine(s). It can also send alerts to the administrator of the machine if it detects an abnormal behaviour.
   + In the case of the ELK-Stack example, *Kibana* is the Web GUI

The inclusion of an agent in this architecture is very useful, as it allows to add an new machine to the system very easily: simply by installing the agent program on the new monitored machine. However, this would make an automatic deployment of the whole system more difficult, as it would require the deployment process to be able  to install the agents on each target remotely, i.e. require administrative rights on all the target machines.

As a consequence, to simplify your future deployment phase, we will simplify this schema a little bit: instead of installing an agent on every monitored machine to collect and send data to a centralized database, we will consider that there's a unique "agent" on this same centralized machine that will connect to each monitored machine through ssh connexions to collect each piece of data.

Your final solution will therefore have an architecture that look more like this:

<img alt="Architecture of your solution" src="./imgs/wanted_architecture.png" width="45%"/>

[^turnkey] "Clef en main".

# Outline of the Labs (and links to the detailed pages)

* [Create the architecture for the Git repository + Create the GitLab Pipeline](./01_Intro.md)
* [Write scripts to get CPU/RAM info from remote machines](./02_Scripts1.md)
* [Activate & write tests for above code](03_Tests1.md)
* [Write scripts to extract info from LOGs on remote machines (with RegExes)](04_Scripts2.md)
* [Write tests for LOG part](05_Tests2.md)
* [Lint / Coverage](06_LintCoverage.md)
* [Framework Web + Admin pour tous *2](07_WebUI.md)
* [Docker *2](08_Docker.md)
* [Deployment *2](09_Deploy.md)
* [Soutenance](Defense.md)


## Resources

Here are some additional resources to help you during this course:

* [DZone's "Continuous Integration Patterns & AntiPatterns"](https://dzone.com/storage/assets/11565134-dzone-refcard84-cipatternsandantipatterns.pdf)
* [DZone's "Continuous Delivery Patterns & AntiPatterns"](https://dzone.com/storage/assets/10862832-dzone-refcard145-cdpatternsandantipatterns.pdf)
* [DZone's "Continuous Integration Servers & Tools"](https://dzone.com/storage/assets/4055325-dzone-refcard087-ci-serversandtools.pdf)
* [DZone's "Continuous Integration/DElivery with Containers"](https://dzone.com/storage/assets/10892980-dzone-refcard276-cicdwithcontainers.pdf)

* [ELK-Stack](https://www.elastic.co/what-is/elk-stack)
